/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    map: {
        '*': {
            'Magento_Ui/js/grid/data-storage': 'Aimtell_WebPushNotifications/js/grid/data-storage'
        }
    }
};
