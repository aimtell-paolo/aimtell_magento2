<?php

namespace Aimtell\WebPushNotifications\Controller\Adminhtml\Notification;


use Magento\Backend\App\Action;

class Add extends \Aimtell\WebPushNotifications\Controller\Adminhtml\AbstractAction
{
    /**
     * @var \Aimtell\WebPushNotifications\Model\NotificationFactory
     */
    private $notificationFactory;
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    private $resultPageFactory;

    public function __construct(Action\Context $context,
                                \Magento\Framework\View\Result\PageFactory $resultPageFactory,
                                \Aimtell\WebPushNotifications\Model\NotificationFactory $notificationFactory)
    {
        parent::__construct($context);
        $this->notificationFactory = $notificationFactory;
        $this->resultPageFactory = $resultPageFactory;
    }

    public function execute()
    {
        return $this->resultPageFactory->create();
    }
}