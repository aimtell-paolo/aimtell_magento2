<?php

namespace Aimtell\WebPushNotifications\Controller\Adminhtml\Notification;

class Index extends \Aimtell\WebPushNotifications\Controller\Adminhtml\AbstractAction
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory)
    {
        $this->resultPageFactory = $resultPageFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        return $this->resultPageFactory->create();
    }

}
