<?php

namespace Aimtell\WebPushNotifications\Controller\Adminhtml;


abstract class AbstractAction extends \Magento\Backend\App\Action
{

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Aimtell_WebPushNotifications::top');
    }

}