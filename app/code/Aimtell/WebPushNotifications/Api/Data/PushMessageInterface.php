<?php

namespace Aimtell\WebPushNotifications\Api\Data;


interface PushMessageInterface
{

    public function getEndPoint();

    public function getPayload();

    public function getTitle();

}