<?php

namespace Aimtell\WebPushNotifications\Api;


use Aimtell\WebPushNotifications\Api\Data\NotificationInterface;

interface PushInterface
{

    public function push(NotificationInterface $notification);

}