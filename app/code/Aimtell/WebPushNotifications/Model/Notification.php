<?php
namespace Aimtell\WebPushNotifications\Model;

use Aimtell\WebPushNotifications\Api\Data\NotificationInterface;
use Magento\Framework\Model\AbstractModel;

class Notification extends AbstractModel implements NotificationInterface
{

    protected function _construct()
    {
        $this->_init('Aimtell\WebPushNotifications\Model\ResourceModel\Notification');
    }


   public function setType($type){
        $this->setData('type',$type);
   }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->getData('title');
    }



    public function setPayload($payload){
        if(is_array($payload)){
            $this->setData('payload',json_encode($payload));
        }else{
            $this->setData('payload',$payload);
        }

    }


    /**
     * Return a JSON encoded string
     * @return string
     */
    public function getPayload()
    {
        return $this->getData('payload');
    }
}
