<?php
namespace Aimtell\WebPushNotifications\Model\ResourceModel\Notification;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Aimtell\WebPushNotifications\Model\Notification','Aimtell\WebPushNotifications\Model\ResourceModel\Notification');
    }
}
