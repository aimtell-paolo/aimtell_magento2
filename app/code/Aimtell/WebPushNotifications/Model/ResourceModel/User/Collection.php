<?php
namespace Aimtell\WebPushNotifications\Model\ResourceModel\User;
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Aimtell\WebPushNotifications\Model\User','Aimtell\WebPushNotifications\Model\ResourceModel\User');
    }
}
