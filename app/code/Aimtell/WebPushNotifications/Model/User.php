<?php
namespace Aimtell\WebPushNotifications\Model;


class User extends \Magento\Framework\Model\AbstractModel
{

    protected function _construct()
    {
        $this->_init('Aimtell\WebPushNotifications\Model\ResourceModel\User');
    }


}
